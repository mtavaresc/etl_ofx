from datetime import datetime
from glob import glob
from os import path, getcwd

from untangle import parse

folder = path.join(getcwd(), 'xml')


def conciliar_ofx(valor_evento, f=0):
    for xml in glob(path.join(folder, '*.xml')):
        root = parse(xml)
        nfe = root.nfeProc.NFe.infNFe

        nnf = nfe.ide.nNF.cdata
        emissao = nfe.ide.dhEmi.cdata
        datahoraemissao = datetime.strptime(emissao, '%Y-%m-%dT%H:%M:%S-03:00')
        valor_nfe = nfe.total.ICMSTot.vNF.cdata

        if valor_nfe == abs(valor_evento):
            print('\nNF {}'.format(nnf))
            print('Emissão: {}'.format(datahoraemissao.strftime('%d/%m/%Y %H:%M:%S')))
            print('R$ {}'.format(valor_nfe))
            f = 1
            break

    if f == 1:
        return True
    else:
        return False
