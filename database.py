from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine('mysql+pymysql://root:@localhost:3306/tester', convert_unicode=True)

Session = sessionmaker(bind=engine)

Base = declarative_base(bind=engine)
