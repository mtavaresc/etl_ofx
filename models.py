from sqlalchemy import Column, Date, String, DECIMAL, ForeignKey, PrimaryKeyConstraint
from sqlalchemy.orm import relationship

from database import Base


class Account(Base):
    __tablename__ = 'account'

    id = Column(String(32), primary_key=True)
    banco = Column(String(3))
    agencia = Column(String(6))
    conta = Column(String(7))

    def __init__(self, key, banco, agencia, conta):
        self.id = key
        self.banco = banco
        self.agencia = agencia
        self.conta = conta


class Transactions(Base):
    __tablename__ = 'transactions'

    account_id = Column(String(32), ForeignKey('account.id'))
    data = Column(Date)
    memo = Column(String(250))
    valor = Column(DECIMAL(11, 2))

    account = relationship('Account', foreign_keys=[account_id])
    PrimaryKeyConstraint(data, memo, valor, account_id, name='transaction_pk')

    def __init__(self, account_id, data, memo, valor):
        self.account_id = account_id
        self.data = data
        self.memo = memo
        self.valor = valor
