import codecs
import os
from datetime import datetime
from hashlib import md5

from ofxparse import OfxParser

from database import Session
from models import Account, Transactions
from reader_nfe import conciliar_ofx

folder = os.path.join(os.getcwd(), 'ofx')

if __name__ == '__main__':
    file = os.path.join(folder, '22993 julho.ofx')

    with codecs.open(file, encoding='iso-8859-1') as fileobj:
        ofx = OfxParser.parse(fileobj)

    account = ofx.account
    institution = account.institution

    banco = institution.fid
    agencia = account.branch_id
    conta = account.account_id

    print("\n### Dados da Conta ###")
    print("\nID Banco: {}".format(banco))
    print("Banco: {}".format(institution.organization))
    print("Agência: {}".format(agencia))
    print("Nº Conta: {}".format(conta))
    print("Tipo de Conta: {}".format(account.type))

    statement = account.statement

    print("\nPeríodo Extrato: {} a {}".format(datetime.strftime(statement.start_date, "%d/%m/%Y"),
                                              datetime.strftime(statement.end_date, "%d/%m/%Y")))
    print("Data saldo: {}".format(datetime.strftime(statement.balance_date, "%d/%m/%Y")))
    print("Saldo atual: {}".format(statement.balance))

    key = md5((banco + agencia + conta).encode('utf-8')).hexdigest()
    s = Session()

    try:
        a = Account(key, banco, agencia, conta)
        s.merge(a)
        s.commit()
    except Exception as e:
        print(format(e))

    print("\n### Transações ###")

    for transaction in statement.transactions:
        data = datetime.strftime(transaction.date, "%Y-%m-%d")
        memo = transaction.memo
        valor = transaction.amount
        # category = memo.split(" - ")[0]
        # description = "" if " - " not in memo else memo.split(" - ")[1]

        print("\n{}".format(datetime.strftime(transaction.date, "%d/%m/%Y")))
        print("Memo: {}".format(memo))
        print("Valor: R$ {}".format(valor))

        s.merge(Transactions(key, data, memo, valor))
        s.commit()

        if conciliar_ofx(valor):
            input('\n\nPress any key to continue...')

    s.close()
